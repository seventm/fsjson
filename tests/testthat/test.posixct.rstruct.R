testthat::context('fsjson - POSIXct.to.rstruct')

testthat::test_that("POSIXct.rstruct", {
    obj <- c(Sys.time(), Sys.time() + 10L)
    rstruct <- fsjson:::POSIXct.to.rstruct(obj)
    testthat::expect_true(fsjson:::isRstruct(rstruct))
    testthat::expect_identical(
      as.character(fsjson:::POSIXct.from.rstruct(rstruct[['__R_struct__']])),
      as.character(obj)
    )
  }
)

